# dont run converter if .no files are present
if [ [ -f .no-composer-yaml ] || [ -f .no-composer-yml ] || [ -f .no-yaml-convert ] ]
then
    php composer $@
    exit
fi

# change file from yml to yaml
if [ [ -f composer.yml ] ]
then
    mv composer.yml composer.yaml
fi

# if no json and no yaml file exist, do nothing
if [ [ ! -f composer.json ] && [ ! -f composer.yaml ] ]
then
    php composer $@
    exit
fi

# do convert
if [ -f composer.yaml ]
then
    echo converting to json
    if [ ! -f composer.json ]
    then
        touch composer.json
    fi
    php yaml-convert.phar composer.yaml composer.json
else
    echo converting to yaml
    if [ ! -f composer.yaml ]
    then
        touch composer.yaml
    fi
    php yaml-convert.phar composer.json composer.yaml
fi

php composer $@

