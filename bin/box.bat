@ECHO OFF

call %~dp0\yaml-convert.bat %~n0
call %~dp0\yaml-convert-dist.bat %~n0

if exist %CD%/bin/%~n0.bat (
	call .\bin\%~n0.bat %*
) else (
	if exist %CD%/vendor/bin/%~n0.bat (
        call .\vendor\bin\%~n0.bat %*
    ) else (
        php %~dp0%~n0.phar %*
    )
)

:eof
