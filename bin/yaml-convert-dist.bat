@ECHO OFF

set workFilename=%1
set config_has_dist_file=no

if exist %workFilename%.yml.dist (
    mv %workFilename%.yml.dist %workFilename%.yaml.dist
)
if exist %workFilename%.yaml.dist (
    set config_has_dist_file=yes
    mv %workFilename%.yaml.dist %workFilename%-dist.yaml
)
if exist %workFilename%.json.dist (
    set config_has_dist_file=yes
    mv %workFilename%.json.dist %workFilename%-dist.json
)


if defined config_has_dist_file (
    call %~dp0\yaml-convert.bat %workFilename%-dist
    if exist %workFilename%-dist.yaml (
        mv %workFilename%-dist.yaml %workFilename%.yaml.dist
    )
    if exist %workFilename%-dist.json (
        mv %workFilename%-dist.json %workFilename%.json.dist 
    )
)