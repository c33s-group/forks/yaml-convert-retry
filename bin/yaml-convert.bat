@ECHO OFF

SET convertScript=yaml-convert.phar
SET workFilename=%1

rem dont run converter if .no files are present
if exist .no-%workFilename%-yaml (
    goto eof
)
if exist .no-%workFilename%-yml (
    goto eof
)
if exist .no-yaml-convert (
    goto eof
)

rem change file from yml to yaml
if exist %workFilename%.yml (
    mv %workFilename%.yml %workFilename%.yaml
)

rem if no json and no yaml file exist, do nothing
if not exist %workFilename%.yaml (
    if not exist %workFilename%.json (
        rem echo "no %workFilename% file exists (json, yaml)"
        goto eof
    )
)

rem do convert
if exist %workFilename%.yaml (
    echo converting %workFilename%.yaml to json
    if not exist %workFilename%.json (
        touch %workFilename%.json
    )
    php %~dp0%convertScript% %workFilename%.yaml %workFilename%.json
) else (
    echo converting %workFilename%.json to yaml
    if not exist %workFilename%.yaml (
        touch %workFilename%.yaml
    )
    php %~dp0%convertScript% %workFilename%.json %workFilename%.yaml
)

:eof
